# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
export PATH="$PATH:/usr/lib/dart/bin"

## Device version matrix
export AB_DEVICES="berlin berlna dubai xpeng coral flame sargo bonito aura taimen walleye nash messi pro1 cheryl mata lake evert payton beckham chef PL2 DRG river channel ocean sailfish marlin troika kane pstar nio tangorpro"
export _21_list="ingot tangorpro berlin pstar pro1x coral TP1803 sargo aura taimen pro1 messi nash cheryl mata lake PL2 river sailfish h872 h918 deadpool wade dopinder sabrina radxa0 m5 odroidc4 radxa0_tab m5_tab odroidc4_tab troika foster sif sphynx nx nx_tab gsi_arm64 gsi_tv_arm64 gsi_car_arm64 gsi_arm gsi_tv_arm gsi_car_x86_64 nio dubai berlna xpeng flame bonito walleye evert payton beckham DRG ocean marlin h990 h830 h850 h910 ls997 rs988 us996 us996d vs995 h870 us997 kane"
export _20_list="coral flame beast"
export _19_list=""
export _18_list="griffin addison ether shamu hltetmo klteactivexx bacon victara m8 jflteatt jfltevzw debx loki arm64 arm64_ab tv_arm64 arm arm_ab tv_arm albus hlte hltekor hltechn klte kltechn kltechnduo klteduos kltedv kltekdi klteaio kltekor m8d jfltexx jfltespr jactivelte jfvelte flox"
export _17_list=""
export _16_list="yellowstone berkeley charlotte"
export _15_list="fugu"
export _14_list=""
export _13_list="molly molly_tab"
export _21_local_list="ingot tangorpro berlin pstar pro1x coral TP1803 sargo aura taimen pro1 messi nash cheryl mata lake PL2 river sailfish h872 h918 deadpool wade dopinder sabrina radxa0 troika foster nx_tab sphynx"
export _20_local_list=""
export _19_local_list=""
export _18_local_list="griffin addison ether shamu hltetmo klteactivexx bacon victara m8 jflteatt jfltevzw debx loki"
export _16_local_list="yellowstone berkeley"
export gms_minimal="coral TP1803 sargo aura taimen pro1 messi cheryl mata lake PL2 river sailfish h918 wade dopinder beast radxa0_tab m5_tab odroidc4_tab troika foster sif gsi_arm64 gsi_tv_arm64 gsi_car_arm64 gsi_arm gsi_tv_arm flame bonito walleye evert payton beckham chef DRG ocean kane bacon debx flox"
export gms_car="car_arm64" # no GMS packages for car_x86_64 gsi_car_arm64 gsi_car_x86_64
export gms_tv="deadpool dopinder wade sabrina beast radxa0 m5 odroidc4 foster sif nx loki molly fugu tv_arm tv_arm64 tv_arm_ab tv_arm64_ab gsi_tv_arm gsi_tv_arm64"
export no_gms="car_x86_64 gsi_car_arm64 gsi_car_x86_64"
export needs_forced_permissive="loki sphynx"
export build_list="16"

## Device serial number matrix
export addison="ZY223VKCCP"
export aura="181849V01501695"
export bacon="60a2774b"
export beast="GZ18020181500069"
export berkeley="3SP0218201000471"
export berlin="ZY22CVZL5G"
export cheryl="RCLLHMA7A2703080"
export coral="92UBA06694"
export debx="08ef8a5f"
export deadpool="AUSA000007FB"
export dopinder="OUSA2124012723"
export ether="NBQGLMB651602338"
export foster2="0324917003807" # darcy
export foster="1321920049104" # mdarcy
export fugu="48F1ADDD"
export griffin="ZY224X27F4"
export h872="LGH8726391a1a4"
export h918="LGH918ac4f860a"
export hltetmo="6adcb60e"
export icosa="3697130306"
export ingot="O1N1XT532200203"
export jflteatt="R31D8086NMH"
export jfltevzw="8813ec98"
export klteactivexx="8fb9be90"
export lake="ZY226H736H"
export loki="0321415800085000000f"
export m8="FA444WM00252"
export mata="PM1GLE4752100528"
export messi="ZY22562RN2"
export molly="ZW2Z14230BC3"
export molly_tab="AD3Z141001E4"
export nash="ZY224BWD3P"
#export nash2="ZY224CV3S2" # broken secureboot off board
export nx_tab="49473587FF3A1BFA12"
export PL2="PL2GARH851901286"
export pro1="a7423fa7"
export pro1x="65d87756"
export pstar="ZY22CXK5K3"
export radxa0="1234567890"
export river="ZY225G673J"
export sabrina="0A311HFDD0V46V"
export sailfish="FA6C10304357"
export sargo="98HAY14YS7"
export shamu="b6943f5"
#export sif="1424319022161"
export sif="10.10.0.50:5555"
export sphynx="5B04002477"
export taimen="710KPKN0286245"
export tangorpro="3407105H803T6G"
export TP1803="88434f8a"
export troika="ZY326VDPDF"
export victara="LX2Z1B0204"
export wade="WUSA2050007356"
export yellowstone="CEIPBP5A0439001240"

## Enviorment Variables
export CCACHE_DIR=/mnt/out/.ccache
export CCACHE_EXEC=$(which ccache)
export EXPERIMENTAL_USE_JAVA8=true
export USE_CCACHE=1
export LC_ALL=C
#export KERNEL_LTO=thin
export WITH_GMS=true
export GMS_MAKEFILE=gms.mk

## Shortcuts
alias adbr="adb root && adb remount && adb shell"
alias commithooks="curl -Lo .git/hooks/commit-msg https://review.lineageos.org/tools/hooks/commit-msg && chmod +x .git/hooks/commit-msg"
alias gca="git commit --amend"
alias gcp="git cherry-pick"
alias gcpm="git cherry-pick -m1"
alias grc="git rebase --continue"
alias ods="ssh ods.ninja"
alias ddt="ssh 10.10.0.216"
alias repos="repo sync --force-sync -j64 && repo forall external/chromium-webview/prebuilt/* -c 'git lfs pull'"
alias reposy="repo sync --force-sync -j64"

git-unfuck () {
    git reset --hard
    git clean -df
    if [ -d ".git/rebase-apply" ] || [ -d ".git/rebase-merge" ]; then
        git rebase --abort
    fi
}

function repopickchain() {
    `python3 ${HOME}/bin/tools/generic/gen_chain.py $@`
}

function rc() {
    echo `python3 ${HOME}/bin/tools/generic/gen_chain.py $@ | cut -b10-`
}

if [[ "$OSTYPE" =~ ^darwin ]]; then
  ## Android Variables
  export ANDROID_HOME=/usr/local/share/android-sdk
  export ANT_HOME=/usr/local/opt/ant
  export CPPFLAGS=-I/usr/local/opt/binutils/include
  export MAVEN_HOME=/usr/local/opt/maven
  export GRADLE_HOME=/usr/local/opt/gradle
  export ANDROID_HOME=/usr/local/Caskroom/android-sdk/4333796
  export ANDROID_NDK_HOME=/usr/local/Caskroom/android-ndk
  export CPPFLAGS=-I/usr/local/opt/binutils/include

  ## Disable annoying bash deprecation warning
  export BASH_SILENCE_DEPRECATION_WARNING=1

  ## PATH Modifications
  export PATH=$ANT_HOME/bin:$PATH
  export PATH=$MAVEN_HOME/bin:$PATH
  export PATH=$GRADLE_HOME/bin:$PATH
  export PATH=$ANDROID_HOME/tools:$PATH
  export PATH=$ANDROID_NDK_HOME/19/android-ndk-r19:$PATH
  export PATH=/Users/nolenjohnson/bin:$PATH
  export PATH=/Users/nolenjohnson/bin/generic:$PATH
  export PATH=/Users/nolenjohnson/bin/macos:$PATH
  export PATH=/usr/local/opt/binutils/bin:$PATH
  export PATH="/usr/local/opt/grep/libexec/gnubin:$PATH"
  export PATH="/usr/local/opt/findutils/libexec/gnubin:$PATH"
  export PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"
  export PATH="/usr/local/opt/binutils/bin:$PATH"
  export PATH="/usr/local/opt/qt/bin:$PATH"
  export PATH="/usr/local/opt/qt/bin:$PATH"

  ## Shortcuts
  alias l="ls -CF"
  alias la="ls -A"
  alias ll="ls -alF"
  alias brew-update="brew update && brew upgrade && brew cleanup"

  ## Terminal Colors
  export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "
  export CLICOLOR=1
  export LSCOLORS=ExFxBxDxCxegedabagacad
fi

if [[ "$OSTYPE" =~ ^linux ]]; then
  ## Log directory for production builds
  export LOGDIR=/var/log/android-builds

  ## PATH Modifications
  export PATH=/home/nolenjohnson/bin:/home/nolenjohnson/bin/scripts:/home/nolenjohnson/bin/jars:/home/nolenjohnson/bin/tools/linux-86/:/home/nolenjohnson/bin/tools/generic:/home/nolenjohnson/.cargo/bin:"$PATH"

  ## Production build server configurations
  export LINEAGE_EXTRAVERSION=ODS-Production
  source  ~/.bashrc-secure
  export ODS_GITHUB_TOKEN="$ODS_GITHUB_TOKEN_SECURE"
  export OUT_DIR_COMMON_BASE=/mnt/out/out

  ## Shortcuts
  alias pkg-update="sudo apt update && sudo apt -y full-upgrade && sudo apt -y autoremove"
fi
